

$(document).ready(function(){

    console.log("Hello jQuery");

    //document.getElementById();

    $("#mybox").html("<p>Hello Vladimir!</p>");

    $('form :password').on('keydown', function(event){
        console.log(event);
        //console.log("clicked: " + event.keyCode);
        if (event.altKey == true 
            && event.ctrlKey == true 
            && event.key == "Shift")
        {
            console.log("you clicked ALt + " + event.keyCode);
        }
    });

    $('#frmCheckbox').on('change', function(){
        //console.log('checkbox changed ');
        let isCheced = this.checked;
        console.log('chkBox: ' + isCheced);
        if (isCheced == true) {
            $('.mylist :nth-child(3)').css('color', 'blue');
        } else {
            $('.mylist :nth-child(3)').css('color', 'red');
        }

    });


    $('#startAjax').on('click', function(){

        console.log ('ajax start');
        
        $.ajax({
            //type: "POST",
            url: "https://developer.mozilla.org",
            crossDomain: true,
            //data: "data",
            dataType: "html",
            success: function (response) {
                console.log('success');
                $("#content").html(response);
            }
        });
    
    });

    

});

function changeColor() {
    //$("#mybox").addClass("red");
    // $("#mybox").css('background-color', 'rgb(15, 48, 141)');
    // chaining
    $("div[id=mybox]")
        .html('<p>Hello Vladimir!</p>')
        .addClass('myFont')
        .css('color', 'red')
        .css('padding', '10px');

    $('#mytable tr:odd').css('color', 'green');
    $('#mytable tr:even').css('color', 'red');

    $('.mylist :last-child').css('color', 'red');
    $('.mylist :first-child').css('color', 'green');
    $('.mylist :nth-child(3)').css('color', 'blue');

    $('form :text').addClass('frmText');
    $('form :password').addClass('frmPassword');
    $('form :submit').addClass('frmButton');
}

